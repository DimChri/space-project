﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movement : MonoBehaviour
{
    [Header("Movement Settings")]
    public float speed = 10f;
    public float rotation_speed = 10f;
    
    private Rigidbody _playerRB;
    private CharacterController _playerCC;
    
    // Start is called before the first frame update
    void Start()
    {
        _playerRB = GetComponent<Rigidbody>();
        _playerCC = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update(){
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        
        Vector3 forward = transform.forward * (vertical * speed * Time.deltaTime);
        Vector3 right = transform.right * (horizontal * speed * Time.deltaTime);
        
        _playerCC.Move(forward + right);

        if (Input.GetMouseButton(2)){
            transform.Rotate(0, Input.GetAxis("Mouse X") * rotation_speed * Time.deltaTime, 0);
        }
    }
}
