﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraController : MonoBehaviour
{

    private Camera camera;

    [Header("Camera Speed Settings")] 
    public float orbitSpeed = 10.0f;
    public GameObject orbit_around;
    
    private float X;
    private float Y;
    private float x_axis;

    private float yaw = 0.0f;
    private float pitch = 0.0f;
    
    // Start is called before the first frame update
    void Start()
    {
        camera = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(2)) {
            x_axis = Input.GetAxis("Mouse X");
            if(x_axis > 0)
                transform.RotateAround(orbit_around.transform.position, Vector3.up, -orbitSpeed * Time.deltaTime);
            else if(x_axis < 0)
                transform.RotateAround(orbit_around.transform.position, Vector3.up, orbitSpeed * Time.deltaTime);

            /*transform.Rotate(new Vector3(Input.GetAxis("Mouse Y") * speed, -Input.GetAxis("Mouse X") * speed, 0));
            X = transform.rotation.eulerAngles.x;
            Y = transform.rotation.eulerAngles.y;
            transform.rotation = Quaternion.Euler(X, Y, 0);
            */

            /*yaw += horizontal_speed * Input.GetAxis("Mouse X");
            pitch -= vertical_speed * Input.GetAxis("Mouse Y");
            gameObject.transform.eulerAngles = new Vector3(pitch, yaw, 0.0f);*/
        }
    }
}
